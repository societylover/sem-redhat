package redhat

import redhat.model.contentManager.ImageContentManager
import redhat.router.CURRENT_MENU_KEY
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*

class PropertySetter {

    fun setEnvironmentProps() {
        try {
            val prop = Properties()
            val propFileName = "config.properties"
            val inputStream = javaClass.classLoader.getResourceAsStream(propFileName)
            if (inputStream != null) {
                prop.load(inputStream)
            } else {
                throw FileNotFoundException("property file '$propFileName' not found in the classpath")
            }

            ImageContentManager.setFilesPath(prop.getProperty("FILES_DIR"))
            System.setProperty("FILES_DIR", prop.getProperty("FILES_DIR"))
            System.setProperty(CURRENT_MENU_KEY, prop.getProperty("CURRENT_MENU_GUID"))
            System.setProperty("DB_PASSWORD", prop.getProperty("DB_PASSWORD"))
            System.setProperty("DB_SCHEMA", prop.getProperty("DB_SCHEMA"))
            System.setProperty("DB_USER", prop.getProperty("DB_USER"))
            System.setProperty("JDBC_DATABASE_URL", prop.getProperty("JDBC_DATABASE_URL"))
            System.setProperty("JDBC_DRIVER", prop.getProperty("JDBC_DRIVER"))
            System.setProperty("JWT_SECRET", prop.getProperty("JWT_SECRET"))
            System.setProperty("SECRET_KEY", prop.getProperty("SECRET_KEY"))

        } catch (e: Exception) {
            println("Exception: $e")
        }
    }

//    fun writeProperty(key: String, value: String) {
//        val prop = System.getProperties()
//        val propFileName = "config.properties"
//        val inputStream = javaClass.classLoader.getResourceAsStream(propFileName)
//        prop.setProperty (key, value)
//        if (inputStream != null) {
//            val out: OutputStream = FileOutputStream(inputStream)
//            prop.store(out, "some comment")
//        } else {
//            throw FileNotFoundException("property file '$propFileName' not found in the classpath")
//        }
//    }

}