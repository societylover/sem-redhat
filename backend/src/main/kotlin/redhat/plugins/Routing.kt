package redhat.plugins

import io.ktor.server.application.*
import io.ktor.server.http.content.*
import io.ktor.server.plugins.swagger.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import redhat.model.repository.*
import redhat.router.*
import redhat.service.JwtService
import java.io.File

fun Application.configureRouting(
    userRepository: UserRepository,
    menuRepository: MenuRepository,
    clientRepository: ClientRepository,
    promoRepository: PromoRepository,
    orderRepository: OrderRepository,
    reportRepository: ReportRepository,
    jwtService: JwtService,
    hashFunction: (String) -> String
) {

    routing {

        staticFiles("/files", File("files"))

        authRoute(
            userRepository = userRepository,
            jwtService = jwtService,
            hashFunction = hashFunction)

        menuRoute(
            menuRepository = menuRepository
        )

        cartRoute(
            menuRepository = menuRepository
        )

        clientRoute(
            clientRepository = clientRepository
        )

        orderRoute(
            orderRepository = orderRepository,
            reportRepository = reportRepository
        )

        reportRoute(
            reportRepository = reportRepository
        )

        promoRoute(
            promoRepository = promoRepository
        )

        get("/") {
            call.respondText("Use /swagger to get API documentation")
        }

        swaggerUI(path = "/swagger", swaggerFile = "openapi/documentation.yaml")

    }
}