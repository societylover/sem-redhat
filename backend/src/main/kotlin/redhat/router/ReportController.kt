package redhat.router

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import redhat.model.repository.ReportRepository
import io.ktor.server.resources.post
import io.ktor.server.resources.get
import io.ktor.server.response.*
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import redhat.API_VERSION
import redhat.model.contentManager.ImageContentManager
import redhat.model.contentManager.ImageContentManager.calculateFileName
import redhat.model.dto.userId
import java.util.*

const val REPORT = "$API_VERSION/report"


@Resource(REPORT)
class ReportRoute

fun Route.reportRoute(
    reportRepository: ReportRepository
) {
    authenticate("report") {
        get<ReportRoute> {
            val principal = call.principal<JWTPrincipal>()!!
            return@get call.respond(HttpStatusCode.OK, reportRepository.getUserReports(principal.userId()))
        }

        post<ReportRoute> {
            val principal = call.principal<JWTPrincipal>()!!

            val multipart = call.receiveMultipart()
            val images = mutableListOf<Pair<String, PartData.FileItem>>()
            lateinit var reportData: String
            var reportVerdict: String ?= null

            try{
                multipart.forEachPart { partData ->
                    when(partData) {
                        // Text fields
                        is PartData.FormItem -> {
                            if (partData.name == "verdict") {
                                reportVerdict = partData.value
                            }
                            else if (partData.name == "data") {
                                reportData = partData.value
                            }
                        }
                        is PartData.FileItem -> { images.add(Pair(calculateFileName(partData), partData)) }
                        // Binary items
                        else -> { }
                    }
                }

                suspendedTransactionAsync {

                    val reportFolderName = reportRepository.createReport(
                        userGUID = principal.userId(),
                        text = reportData,
                        photoUris = images.map { it.first },
                        result = reportVerdict
                    )

                    ImageContentManager.saveFiles(images, reportFolderName)
                    call.respond(HttpStatusCode.Created)
                }.await()

            } catch (ex: Exception) {
                call.respond(HttpStatusCode.InternalServerError,"Create file error")
            }
        }
    }
}