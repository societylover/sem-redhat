package redhat.router

import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import redhat.API_VERSION
import redhat.model.dto.userRole
import redhat.model.enums.Role
import redhat.model.repository.MenuRepository
import io.ktor.server.resources.post

const val CART = "$API_VERSION/cart"
const val CART_PRICING = "$CART/pricing"

@Resource(CART_PRICING)
class CartPricingRoute

fun Route.cartRoute(
    menuRepository: MenuRepository
) {

    authenticate("jwt") {
        post <CartPricingRoute> {

            val role = call.principal<JWTPrincipal>()!!.userRole()

            if (role != Role.Client) {
                call.respond(HttpStatusCode.BadRequest, "Bad user role")
                return@post
            }

            val cartParameters = call.receive<List<String>>()

            if (cartParameters.isEmpty()) {
                return@post
            }

            val calculatedItems = menuRepository.calculateCart(cartParameters)

            return@post call.respond(HttpStatusCode.OK, calculatedItems)
        }
    }

}
