package redhat.router

import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import redhat.API_VERSION
import redhat.model.dto.userRole
import redhat.model.enums.Role
import redhat.model.repository.ClientRepository
import io.ktor.server.resources.post
import io.ktor.server.resources.delete

const val CLIENT = "$API_VERSION/client"
const val CLIENT_ADDRESSES = "$CLIENT/address"
const val CLIENT_ADDRESS_DELETE = "$CLIENT/address/{clientGuid}"

@Resource(CLIENT_ADDRESSES)
class ClientAddressesRoute

@Resource(CLIENT_ADDRESS_DELETE)
class ClientAddressDeleteRoute

fun Route.clientRoute(
    clientRepository: ClientRepository
) {

    authenticate("jwt") {
        get<ClientAddressesRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Client) {
                call.respond(HttpStatusCode.Unauthorized, "Only Clients allowed by this route")
                return@get
            }

            val clientAddresses =
                clientRepository.getClientAddressesByUserGuid(principal.payload.getClaim("id").asString())

            call.respond(HttpStatusCode.OK, clientAddresses)
        }

        post<ClientAddressesRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Client) {
                call.respond(HttpStatusCode.Unauthorized, "Only Clients allowed by this route")
                return@post
            }

            val address = call.receive<String>()

            if (address.isEmpty()) {
                return@post call.respond(HttpStatusCode.MethodNotAllowed, "Missing Fields")
            }

            clientRepository.createClientAddressByUserGuid(
                    userGUID = principal.payload.getClaim("id").asString(),
                    address = address)

            call.respond(HttpStatusCode.Created)
        }

        delete<ClientAddressDeleteRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Client) {
                call.respond(HttpStatusCode.Unauthorized, "Only Clients allowed by this route")
                return@delete
            }

            val clientGuid = call.parameters["clientGuid"] ?: call.respond(HttpStatusCode.BadRequest, "No client passed")

            clientRepository.deleteClientByGuid(clientGuid.toString())
            return@delete call.respond(HttpStatusCode.NoContent)
        }
    }
}