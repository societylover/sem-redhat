package redhat.router

import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.resources.post
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import redhat.API_VERSION
import redhat.model.repository.UserRepository
import redhat.service.JwtService
import io.ktor.server.routing.*
import io.ktor.util.Identity.decode
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import redhat.model.enums.Role

const val AUTH = "$API_VERSION/auth"
const val AUTH_LOGIN = "$AUTH/login"
const val AUTH_REGISTER = "$AUTH/register"

@Resource(AUTH_LOGIN)
class AuthLoginRoute
@Resource(AUTH_REGISTER)
class AuthRegisterRoute

@Serializable
private data class LoginData(
    val login: String,
    val password: String
)

fun Route.authRoute(
    userRepository: UserRepository,
    jwtService: JwtService,
    hashFunction: (String) -> String
) {

    
    post<AuthLoginRoute> {

        val signinData = call.receive<LoginData>()
        println(signinData)

        val hash = hashFunction(signinData.password)
        try {
            val currentUser = userRepository.getUserByCredentials(signinData.login)

            if (currentUser == null) {
                // User with specified login not found! (Create user?)
                call.respond(HttpStatusCode.NotFound)
                return@post
            }

            // User with specified login was found!
            if (currentUser.passwordHash == hash) {
                // Password is same
                call.respond(HttpStatusCode.OK, jwtService.generateToken(currentUser))
            } else {
                // Password is same
                call.respond(HttpStatusCode.BadRequest, "The specified password does not match the user")
            }

        } catch (e: Throwable) {
            call.respond(HttpStatusCode.BadRequest, "Problems retrieving user")
        }
    }

    // Create new user with Client role
    post<AuthRegisterRoute> {
        val signinData = call.receive<LoginData>()
        try {
            val isCurrentUserExist = userRepository.checkIfUserExistByCredentials(signinData.login)

            if (isCurrentUserExist) {
                // User with specified login was found! (Can't create new user!)
                call.respond(HttpStatusCode.NotAcceptable)
                return@post
            }

            val createdUser = userRepository.createUser(
                login = signinData.login,
                passwordHash = hashFunction(signinData.password),
                role = Role.Client
            )

            call.respond(HttpStatusCode.Created, jwtService.generateToken(createdUser))

        } catch (e: Throwable) {
            println(e)

            call.respond(HttpStatusCode.BadRequest, "Problems retrieving user")
        }
    }
}