package redhat.router

import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.routing.*
import redhat.API_VERSION
import redhat.model.repository.PromoRepository
import io.ktor.server.resources.post
import io.ktor.server.resources.put
import io.ktor.server.resources.get
import io.ktor.server.response.*
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import redhat.model.contentManager.ImageContentManager
import redhat.model.dto.userRole
import redhat.model.enums.Role

const val PROMO = "$API_VERSION/promo"
const val PROMO_SETTINGS = "$API_VERSION/promo/settings"

@Resource(PROMO_SETTINGS)
class PromoSettingsRoute

@Resource(PROMO)
class PromoRoute

fun Route.promoRoute(
    promoRepository: PromoRepository
) {

    val promoPath = "${System.getProperty("FILES_DIR")}/promo"

    authenticate("jwt") {
        get<PromoSettingsRoute> {

            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Admin) {
                call.respond(HttpStatusCode.Unauthorized, "Only Admin allowed by this route")
                return@get
            }

            return@get call.respond(HttpStatusCode.OK, promoRepository.getAllPromosData())
        }

        post<PromoSettingsRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Admin) {
                return@post call.respond(HttpStatusCode.Unauthorized, "Only Admin allowed by this route")
            }

            val images = mutableListOf<Pair<String, PartData.FileItem>>()

            try{
                call.receiveMultipart().forEachPart { partData ->
                    when(partData) {
                        is PartData.FileItem -> {
                            images.add(Pair(
                                ImageContentManager.calculateFileName(partData),
                                partData))
                        }
                        // Binary items
                        else -> { }
                    }
                }

                suspendedTransactionAsync {
                    ImageContentManager.saveFiles(images, "promo")
                    promoRepository.addPromoImages(images.map { "$promoPath/${it.first}" })
                    call.respond(HttpStatusCode.Created)
                }.await()

            } catch (ex: Exception) {
                call.respond(HttpStatusCode.InternalServerError,"Create file error")
            }
        }

        put<PromoSettingsRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Admin) {
                call.respond(HttpStatusCode.Unauthorized, "Only Admin allowed by this route")
                return@put
            }

            val promoShownItems = call.receive<List<String>>()

            if (promoShownItems.isEmpty()) {
                return@put call.respond(HttpStatusCode.BadRequest, "No shown items provided")
            }

            return@put call.respond(HttpStatusCode.OK, promoRepository.setShownPromoItems(promoShownItems))
        }

        get<PromoRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Client) {
                call.respond(HttpStatusCode.Unauthorized, "Only Client allowed by this route")
                return@get
            }

            return@get call.respond(HttpStatusCode.OK, promoRepository.getCurrentPromoItemsPath())
        }
    }
}