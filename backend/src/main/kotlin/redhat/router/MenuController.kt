package redhat.router

import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.routing.*
import io.ktor.server.resources.get
import io.ktor.server.resources.put
import io.ktor.server.response.*
import io.ktor.server.sessions.*
import redhat.API_VERSION
import redhat.model.dto.userRole
import redhat.model.enums.Role
import redhat.model.repository.MenuRepository
import java.io.File
import java.util.*

const val MENU = "$API_VERSION/menu"
const val MENU_LIST = "$API_VERSION/menus"
const val MENU_ID_UPDATE = "$API_VERSION/menus/{menuGUID}"

@Resource(MENU)
class MenuRoute

@Resource(MENU_LIST)
class MenusRoute

@Resource(MENU_ID_UPDATE)
class CurrentMenuUpdateRoute

fun Route.menuRoute(
    menuRepository: MenuRepository
) {
    menuRepository.setCurrentMenu(System.getProperty(CURRENT_MENU_KEY))

    authenticate("jwt") {
        get<MenuRoute> {

            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Client) {
                call.respond(HttpStatusCode.Unauthorized, "Bad user role!")
                return@get
            }

            val menuItems = menuRepository.getCurrentMenuItems()

            call.respond(HttpStatusCode.OK, menuItems)
        }

        get<MenusRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Admin) {
                call.respond(HttpStatusCode.Unauthorized, "Only Admin allowed")
                return@get
            }

            return@get call.respond(HttpStatusCode.OK, menuRepository.getMenus())
        }

        put <CurrentMenuUpdateRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Admin) {
                call.respond(HttpStatusCode.Unauthorized, "Only Admin allowed")
                return@put
            }

            var menuGuid = call.parameters["menuGUID"] ?: call.respond(HttpStatusCode.BadRequest, "No menu guid passed")

            menuGuid = menuGuid.toString()
            System.setProperty(CURRENT_MENU_KEY, menuGuid)

            return@put call.respond(HttpStatusCode.OK, menuRepository.setCurrentMenu(menuGuid))
        }
    }
}

internal const val CURRENT_MENU_KEY = "CURRENT_MENU_GUID"