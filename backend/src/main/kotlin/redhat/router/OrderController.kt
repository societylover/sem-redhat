package redhat.router

import io.ktor.http.*
import io.ktor.resources.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.request.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import redhat.API_VERSION
import io.ktor.server.resources.post
import io.ktor.server.resources.put
import kotlinx.serialization.Serializable
import redhat.model.dto.userId
import redhat.model.dto.userRole
import redhat.model.enums.Role
import redhat.model.enums.StatusEnum
import redhat.model.repository.OrderRepository
import redhat.model.repository.ReportRepository

const val ORDER = "$API_VERSION/order"
const val ORDER_DETAILS = "$ORDER/{orderGuid}"

@Resource(ORDER)
class OrderRoute

@Resource(ORDER_DETAILS)
class OrderDetailRoute

fun Route.orderRoute(
    orderRepository: OrderRepository,
    reportRepository: ReportRepository
) {

    val acceptableGetRoles = setOf(Role.Client, Role.Admin, Role.Cook, Role.Operator, Role.Courier)

    authenticate("jwt") {

        post<OrderRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() != Role.Client) {
                call.respond(HttpStatusCode.Unauthorized, "Only Clients allowed by this route")
                return@post
            }

            try {
                val createdOrderGuid = orderRepository.createOrder(call.receive()) ?: call.respond(HttpStatusCode.BadRequest, "Cant create order")
                call.respond(HttpStatusCode.Created, createdOrderGuid)
            } catch (e: Throwable) {
                call.respond(HttpStatusCode.BadRequest, "Not all data has been sent")
                return@post
            }
        }

        get<OrderRoute> {
            val principal = call.principal<JWTPrincipal>()

            if (principal!!.userRole() !in acceptableGetRoles) {
                return@get call.respond(HttpStatusCode.Unauthorized, "Bad role for this route. Only $acceptableGetRoles allowed")
            }

            when(principal.userRole()) {
                Role.Client -> {
                    return@get call.respond(HttpStatusCode.Found, orderRepository.getOrdersShortData(userId = principal.userId()))
                }
                Role.Admin -> {
                    return@get call.respond(HttpStatusCode.Found, orderRepository.getOrdersShortData(searchingStatuses = StatusEnum.values().toList()))
                }
                Role.Cook -> {
                    return@get call.respond(HttpStatusCode.Found, orderRepository.getOrdersShortData(searchingStatuses = listOf(StatusEnum.Created, StatusEnum.Cooking)))
                }
                Role.Operator -> {
                    return@get call.respond(HttpStatusCode.Found, orderRepository.getOrdersShortData(searchingStatuses = listOf(StatusEnum.Cooking, StatusEnum.WaitingTakeout)))
                }
                Role.Courier -> {
                    val userId = principal.payload.getClaim("id").asString()
                    return@get call.respond(HttpStatusCode.Found, orderRepository.getCourierOrdersShortData(courierId = userId))
                }
                else ->  return@get call.respond(HttpStatusCode.BadRequest, "Unknown role for this request route")
            }
        }

        get<OrderDetailRoute> {
            val principal = call.principal<JWTPrincipal>()

            val orderGuid = call.parameters["orderGuid"] ?: call.respond(HttpStatusCode.BadRequest, "No order id passed")

            if (principal!!.userRole() !in acceptableGetRoles) {
                return@get call.respond(HttpStatusCode.Unauthorized, "Bad role for this route. Only $acceptableGetRoles allowed")
            }

            return@get call.respond(HttpStatusCode.OK, orderRepository.getOrderFullData(orderGuid.toString()))
        }

        put<OrderDetailRoute> {
            val principal = call.principal<JWTPrincipal>()
            var orderGuid = call.parameters["orderGuid"] ?: call.respond(HttpStatusCode.BadRequest, "No order id passed")

            if (principal!!.userRole() !in acceptableGetRoles) {
                return@put call.respond(HttpStatusCode.Unauthorized, "Bad role for this route. Only $acceptableGetRoles allowed")
            }

            orderGuid = orderGuid.toString()

            val method: Method? = try {
                call.receiveNullable<Method>()
            } catch (e: ContentTransformationException) {
                null
            }

            val userId = principal.payload.getClaim("id").asString()

            when(principal.userRole()) {
                Role.Client -> {

                    if (method == null) {
                        return@put call.respond(HttpStatusCode.BadRequest, "No method data passed")
                    }

                    if (method.method.lowercase() == "cancel")
                    {
                        orderRepository.updateOrderStatus(orderGuid = orderGuid, status = StatusEnum.Canceled)
                        return@put call.respond(HttpStatusCode.OK)
                    }

                    if (method.method.lowercase() == "report" && method.data != null)
                    {
                        return@put call.respond(HttpStatusCode.Created, reportRepository.createReport(userGUID = userId, text = method.data, photoUris = listOf()))
                    }

                    return@put call.respond(HttpStatusCode.BadRequest)

                }
                Role.Admin -> {
                    orderRepository.updateOrderStatus(orderGuid = orderGuid, status = StatusEnum.Canceled)
                    return@put call.respond(HttpStatusCode.OK)
                }
                Role.Cook -> {
                    orderRepository.updateOrderStatus(orderGuid = orderGuid, status = StatusEnum.Cooking)
                    return@put call.respond(HttpStatusCode.OK)
                }
                Role.Operator -> {
                    orderRepository.updateOrderStatus(orderGuid = orderGuid, status = StatusEnum.WaitingTakeout)
                    return@put call.respond(HttpStatusCode.OK)
                }
                Role.Courier -> {
                    if (method == null) {
                        return@put call.respond(HttpStatusCode.BadRequest, "No method data passed")
                    }

                    if (method.method.lowercase() == "take")
                    {
                        orderRepository.takeOrderInDelivery(orderGuid = orderGuid, courierId = userId)
                        return@put call.respond(HttpStatusCode.OK)
                    }

                    if (method.method.lowercase() == "issue")
                    {
                        orderRepository.updateOrderStatus(orderGuid = orderGuid, status = StatusEnum.Done)
                        return@put call.respond(HttpStatusCode.OK)
                    }

                    return@put call.respond(HttpStatusCode.BadRequest)
                }

                else -> return@put call.respond(HttpStatusCode.BadRequest, "Unknown role for this request route")
            }

        }
    }
}

@Serializable
internal data class Method(
    val method: String,
    val data: String? = null
)