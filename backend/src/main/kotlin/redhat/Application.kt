package redhat

import io.ktor.http.*
import io.ktor.server.resources.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.response.*
import redhat.model.DatabaseInitialize
import redhat.model.dto.userRole
import redhat.model.dto.userRoleAsString
import redhat.model.enums.Role
import redhat.model.repository.implementations.*
import redhat.plugins.*
import redhat.service.JwtService
import redhat.service.hash

fun main() {

    PropertySetter().setEnvironmentProps()

    embeddedServer(Netty, port = 32228, host = "localhost", module = Application::module)
        .start(wait = true)
}

fun Application.module() {

    DatabaseInitialize.init()

    val jwtService = JwtService()
    val hashFunction = { s: String -> hash(s) }

    install(Resources)

    install(CORS) {
        allowHeader(HttpHeaders.Authorization)
        allowHeader(HttpHeaders.ContentType)
        allowHeader(HttpHeaders.AccessControlAllowOrigin)

        allowHeadersPrefixed("custom-")

        allowHeaders { true }
        HttpMethod.DefaultMethods.forEach { allowMethod(it) }

        allowCredentials = true
        anyHost()
    }

    install(Authentication) {
        jwt("jwt") {
            verifier(jwtService.verifier)
            realm = "Redhat Server"
            validate {
                if (it.userRoleAsString() != "") {
                    JWTPrincipal(it.payload)
                } else {
                    null
                }
            }
            challenge { _, _ ->
                call.respond(HttpStatusCode.Unauthorized, "Token is not valid or has expired")
            }
        }

        jwt("report") {
            val acceptableRoles = setOf(Role.Inspector, Role.SecretShopper)

            verifier(jwtService.verifier)
            realm = "Redhat Server"

            validate {
                if (it.userRole() in acceptableRoles) {
                    JWTPrincipal(it.payload)
                } else {
                    null
                }
            }
            challenge { _, _ ->
                call.respond(HttpStatusCode.Unauthorized, "Token is not valid or has expired")
            }
        }

        jwt("cart") {
            verifier(jwtService.verifier)
            realm = "Redhat Server"

            validate {
                if (it.userRole() != Role.Client) {
                    JWTPrincipal(it.payload)
                } else {
                    null
                }
            }
            challenge { _, _ ->
                call.respond(HttpStatusCode.Unauthorized, "Token is not valid or has expired")
            }
        }
    }

    configureSerialization()
    configureHTTP()

    configureRouting(
        userRepository = UserRepositoryImpl(),
        menuRepository = MenuRepositoryImpl(),
        clientRepository = ClientRepositoryImpl(),
        orderRepository = OrderRepositoryImpl(),
        promoRepository = PromoRepositoryImpl(),
        reportRepository = ReportRepositoryImpl(System.getProperty("FILES_DIR")),
        jwtService = jwtService,
        hashFunction = hashFunction
    )
}

const val API_VERSION = "/v1"