package redhat.model.enums

enum class StatusEnum {
    Created,
    Cooking,
    WaitingTakeout,
    Delivering,
    Done,
    Canceled
}