package redhat.model.enums

enum class PaymentEnum {
    Cash,
    Card
}