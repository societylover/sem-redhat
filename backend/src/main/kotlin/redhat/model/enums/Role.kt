package redhat.model.enums

enum class Role {
    Admin,
    Cook,
    Operator,
    Courier,
    Inspector,
    SecretShopper,
    Client
}