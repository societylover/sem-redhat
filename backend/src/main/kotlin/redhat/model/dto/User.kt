package redhat.model.dto

import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import kotlinx.serialization.Serializable
import redhat.model.enums.Role

@Serializable
data class User(
    val userGUID: String,
    val login: String,
    var userRole: Role,
    val passwordHash: String
) : Principal

fun JWTCredential.userRole() : Role {
    return Role.valueOf(this
        .payload.getClaim("role").asString())
}

fun JWTCredential.userRoleAsString() : String {
    return try {
        this.userRole().toString()
    } catch(e: Exception) {
        ""
    }
}

fun JWTPrincipal.userRole() : Role {
    return Role.valueOf(this
        .payload.getClaim("role").asString())
}


fun JWTPrincipal.userId() : String {
    return this
        .payload.getClaim("id").asString()
}