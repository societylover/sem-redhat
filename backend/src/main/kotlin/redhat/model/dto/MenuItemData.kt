package redhat.model.dto

import kotlinx.serialization.Serializable

@Serializable
data class MenuItemData(
    val menuItemGuid: String,
    val name: String,
    val description: String,
    val photoUri: String,
    val available: Boolean,
    val cost: String,
    val discount: String?
)
