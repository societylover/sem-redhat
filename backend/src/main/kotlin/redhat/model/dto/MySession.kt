package redhat.model.dto

import redhat.model.enums.Role

data class MySession(val userGUID: String, val userRole: Role)