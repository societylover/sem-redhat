package redhat.model

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import redhat.model.enums.Role
import redhat.model.enums.PaymentEnum
import redhat.model.enums.StatusEnum
import redhat.model.tables.*
import java.math.BigDecimal
import java.nio.file.Files
import java.nio.file.Paths

object DatabaseInitialize {

    fun init() {

        val url = System.getProperty("JDBC_DATABASE_URL")
        val driver = System.getProperty("JDBC_DRIVER")
        val user = System.getProperty("DB_USER")
        val password = System.getProperty("DB_PASSWORD")

        Database.connect(url, driver, user, password)


        transaction {
            addLogger(StdOutSqlLogger)

            SchemaUtils.drop(
                Dishes, Menus, Orders, MenuItems, OrderItems,
                ReportPhotos, Reports, Clients, Users, Promos
            )

            SchemaUtils.createMissingTablesAndColumns(
                Dishes, Users, ReportPhotos, Promos,
                Clients, Menus, MenuItems, OrderItems, Orders
            )

            migration()
        }
    }

    // Initial Migration
    private fun Transaction.migration() {

        Files.createDirectories(Paths.get("files/report"))
        Files.createDirectories(Paths.get("files/menu"))
        Files.createDirectories(Paths.get("files/promo"))

        Promos.insert {
            it[Promos.promoImageUri] = "/files/promo/promo1.png"
        }

        Promos.insert {
            it[Promos.promoImageUri] = "/files/promo/promo2.png"
        }

        Promos.insert {
            it[Promos.promoImageUri] = "/files/promo/promo3.png"
            it[Promos.isShown] = true
        }

        Promos.insert {
            it[Promos.promoImageUri] = "/files/promo/promo4.png"
            it[Promos.isShown] = true
        }

        val insertedDishesGuids = listOf(

            Dishes.insert {
                it[dishGUID] = "760745c3-b723-4389-9b7b-2f071ba23512"
                it[name] = "Пирожок мясной «Коронный»"
                it[pictureUrl] = "/files/menu/пирожок_мясо.webp"
                it[description] = "Очень вкусные пирожки из мяса и теста. Бабушкам очень понравятся.\nСостав: Мясо, тесто, секретный ингредиент"
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "ff9de3cc-e669-4fec-b783-46291adc8734"
                it[name] = "Борщ с говядиной"
                it[pictureUrl] = "/files/menu/борщ_говядина.webp"
                it[description] = "Ароматный и густой красный борщ по традиционному рецепту. Заботливо приготовлен на крепком и прозрачном говяжьем бульоне, с щедрой заправкой из сладкой свёклы, моркови, картофеля, капусты и болгарского перца."
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "f6aa0fa5-fb01-490b-a096-0c325d01929c"
                it[name] = "Пирог с капустой и мясом дрожжевой"
                it[pictureUrl] = "/files/menu/пирог_капуста_мясо.webp"
                it[description] = "Румяный пирог с говяжьим фаршем и припущенной белокочанной капустой. Приготовлен по-домашнему: нежное тесто замешано на сливочном масле, а начинки действительно много!"
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "20cc46d5-5b64-4569-8864-7a42fa4307af"
                it[name] = "Пирог осетинский с картофелем и сыром"
                it[pictureUrl] = "/files/menu/пирог_осетинский_картофель_сыр.webp"
                it[description] = "Осетинский национальный пирог на тонком дрожжевом тесте. Начинка из картофельного пюре и осетинского сыра станет ещё вкуснее после подогрева в микроволновке. Большого пирога хватит на целую семью!"
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "895c6f92-c76c-460c-a469-f66d93c58037"
                it[name] = "Пирог «Сметанник» с черникой"
                it[pictureUrl] = "/files/menu/пирог_сметанник_черника.webp"
                it[description] = "Если вы без ума от классического сметанника, вам наверняка понравится и эта его версия. Мягкая тарталетка одновременно напоминает песочное тесто и воздушный бисквит и прекрасно сочетается с обволакивающим сметанным крем-муссом, а черника придаёт вкусу десерта сочную ягодную кислинку."
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "d19fa342-c3d0-4673-9627-30261525b4eb"
                it[name] = "Пирожок с капустой"
                it[pictureUrl] = "/files/menu/пирожок_капуста.webp"
                it[description] = "Печёные пирожки с капустой, изготовленные из домашнего дрожжевого теста. Лепим вручную. Вкус ностальгии по чаепитиям у бабушки. Только натуральные ингредиенты и никакой химии!"
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "3cdbac1e-b929-4214-bf1b-87436df912fa"
                it[name] = "Пирожок с куриной начинкой"
                it[pictureUrl] = "/files/menu/пирожок_курица.webp"
                it[description] = "Аппетитный пирожок, в котором мягкое дрожжевое тесто сочетается с большим количеством нежной начинки из куриного филе. Для сытного перекуса и в качестве дополнения к первым блюдам — то, что нужно."
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "13e60bfa-3a06-4425-9cac-53f4ac8a86f7"
                it[name] = "Пирожок с яблоком"
                it[pictureUrl] = "/files/menu/пирожок_яблоко.webp"
                it[description] = "Пирожок как домашний. У него тонкое тесто и много начинки из свежих яблок. Приготовлен из пшеничной муки высшего сорта с добавлением сливочного масла, на дрожжах."
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "becdb9b6-aafd-428b-a968-f05fe2e09c45"
                it[name] = "Пирожок с зеленью и яйцом"
                it[pictureUrl] = "/files/menu/пирожок_яйцо_зелень.webp"
                it[description] = "Румяный печёный пирожок. Приготовлен из муки высшего сорта с добавлением молока и сливочного масла, на дрожжах. Тесто мягкое и нежное. Много начинки из варёных яиц, петрушки, укропа и зелёного лука."
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "d1597c99-3c79-4aeb-aadb-46e243bec814"
                it[name] = "Суп «Гороховый» с копчёностями"
                it[pictureUrl] = "/files/menu/суп_гороховый.webp"
                it[description] = "Первое блюдо из гороха, картофеля, моркови, репчатого лука, копченоо-вареного свиного окорока, охотничьих колбасок с добавлением подсолнечного масла, соли, черного перца и лаврового листа."
            } get Dishes.dishGUID,

            Dishes.insert {
                it[dishGUID] = "7707577c-9ad8-42b5-bda4-e9db35e57e85"
                it[name] = "Суп «Харчо»"
                it[pictureUrl] = "/files/menu/суп_харчо.webp"
                it[description] = "Классический рецепт этого блюда пришел к нам из грузинской кухни. Приготовлен суп из говяжьего бульона, говядины, риса, томатной пасты и пряностей. В составе только свежие и качественные ингредиенты, без консервантов и усилителей вкуса."
            } get Dishes.dishGUID,
        )

        // https://www.freeformatter.com/hmac-generator.html#before-output
        // Secret key - aM=k3mf^GPE{'4y&UX&|4.h^<5/FhU (You don't see this line!)
        // Digest algo - SHA1

        // Admin
        Users.insert {
            it[userGUID] = "0747f1b3-d09c-4d0c-abba-514c85b9c9f1"
            it[login] = "111111"
            it[role] = Role.Admin
            it[hashedPassword] = "fb3d674016296e3ef0f4fe246aee6ba5a5047be6"
        }

        Users.insert {
            it[userGUID] = "333f0da1-4200-4cb0-93c6-af6acbe755b4"
            it[login] = "222222"
            it[role] = Role.Client
            it[hashedPassword] = "170ce61ceea049f9b9d456b42b8dbe17669d0a86"
        }

        Users.insert {
            it[userGUID] = "6f1df7f0-32d1-40a7-a13d-917d2b8e5957"
            it[login] = "333333"
            it[role] = Role.Courier
            it[hashedPassword] = "d811354e3d17d487621f25cd629c02f62b1deb2d"
        }

        Users.insert {
            it[userGUID] = "ca7c0f3f-503a-4e0c-82f6-a7b29b4da11b"
            it[login] = "444444"
            it[role] = Role.Cook
            it[hashedPassword] = "d2d77b6857583e5ccc1864d5c9f2f4fd3903d174"
        }

        Users.insert {
            it[userGUID] = "7b3cc33f-5e7c-453f-8138-aadb5d2df66b"
            it[login] = "555555"
            it[role] = Role.Inspector
            it[hashedPassword] = "c6d18bb82d2d97639240ffaf7663fd936abfaa91"
        }

        Users.insert {
            it[userGUID] = "df6df839-db76-43fa-9e72-600354ad467a"
            it[login] = "666666"
            it[role] = Role.Operator
            it[hashedPassword] = "b9895c4c287ee595679ebce0c02b6939b639339c"
        }

        Users.insert {
            it[userGUID] = "869a9284-c9be-4b94-b820-ac327bfe5e35"
            it[login] = "777777"
            it[role] = Role.SecretShopper
            it[hashedPassword] = "e475f0631fa2b167fa6f7b2a33b4967b7f7ba05f"
        }

        Reports.insert {
            it[reportGUID] = "2f970bf0-e3d8-45ff-95b5-ae134e0fd402"
            it[text] = "Все грязно и точка"
            it[userGUID] = "0747f1b3-d09c-4d0c-abba-514c85b9c9f1"
            it[result] = "В зеркало посмотрел? Отклонено"
        }

        Reports.insert {
            it[reportGUID] = "8bc049ad-1dee-450f-bf72-e0dbbc8b80af"
            it[text] = "Все чисто и вкусно. Даже не стало плохо"
            it[userGUID] = "333f0da1-4200-4cb0-93c6-af6acbe755b4"
            it[result] = "Обработано"
        }

        ReportPhotos.insert {
            it[reportGUID] = "8bc049ad-1dee-450f-bf72-e0dbbc8b80af"
            it[photoUri] = "1.jpg"
        }

        ReportPhotos.insert {
            it[reportGUID] = "8bc049ad-1dee-450f-bf72-e0dbbc8b80af"
            it[photoUri] = "0.jpg"
        }

        ReportPhotos.insert {
            it[reportGUID] = "2f970bf0-e3d8-45ff-95b5-ae134e0fd402"
            it[photoUri] = "0.jpg"
        }

        ReportPhotos.insert {
            it[reportGUID] = "2f970bf0-e3d8-45ff-95b5-ae134e0fd402"
            it[photoUri] = "1.jpg"
        }

        Menus.insert {
            it[menuGUID] = "228c90d6-ca0b-4ff6-b65e-561fe04b96a0"
            it[name] = "Летнее меню"
        }

        Menus.insert {
            it[menuGUID] = "81f2fa41-6d02-4c3a-92bb-b2b92d177ae3"
            it[name] = "Зимнее меню"
        }

        Clients.insert {
            it[clientGUID] = "1f9fedb3-6705-4926-8868-263fe2b567d3"
            it[userGUID] = "6f1df7f0-32d1-40a7-a13d-917d2b8e5957"
            it[address] = "2-ая Сказочная, 28"
        }

        Clients.insert {
            it[clientGUID] = "6ed49324-73b1-48a4-928e-66c924e3dfb3"
            it[userGUID] = "333f0da1-4200-4cb0-93c6-af6acbe755b4"
            it[address] = "2-ая Сказочная, 228"
        }

        Clients.insert {
            it[clientGUID] = "2fae5b78-f28f-4381-85e7-fbcded4eeb47"
            it[userGUID] = "333f0da1-4200-4cb0-93c6-af6acbe755b4"
            it[address] = "ул. Дагестанская, 13"
        }

        Clients.insert {
            it[clientGUID] = "ae700e15-5d76-4396-b462-3072392278ae"
            it[userGUID] = "6f1df7f0-32d1-40a7-a13d-917d2b8e5957"
            it[address] = "пр. генерала Буратино, 1"
        }

        Clients.insert {
            it[clientGUID] = "06c0821f-e0ce-4a3d-9fd8-4ca26cc19ce0"
            it[userGUID] = "6f1df7f0-32d1-40a7-a13d-917d2b8e5957"
            it[address] = "ул. Долматиновая, 101"
        }

        MenuItems.insert {
            it[menuItemGUID] = "1c6f76d4-3369-4cc4-b498-548fd7469433"
            it[dishGUID] = insertedDishesGuids[0]
            it[menuGUID] = "228c90d6-ca0b-4ff6-b65e-561fe04b96a0"
            it[cost] = BigDecimal("250.10")
        }

        MenuItems.insert {
            it[menuItemGUID] = "c8254d93-491d-4654-935d-779b2b4cb3f7"
            it[dishGUID] = insertedDishesGuids[1]
            it[menuGUID] = "228c90d6-ca0b-4ff6-b65e-561fe04b96a0"
            it[cost] = BigDecimal("300")
            it[discount] = BigDecimal("50")
        }

        MenuItems.insert {
            it[menuItemGUID] = "a58efee1-9ad7-4007-8fcc-1426b0f41a56"
            it[dishGUID] = insertedDishesGuids[2]
            it[menuGUID] = "228c90d6-ca0b-4ff6-b65e-561fe04b96a0"
            it[cost] = BigDecimal("400.50")
            it[discount] = BigDecimal("10")
        }

        MenuItems.insert {
            it[menuItemGUID] = "6ba5f954-5c2c-44a3-80e0-02041994b4aa"
            it[dishGUID] = insertedDishesGuids[3]
            it[menuGUID] = "228c90d6-ca0b-4ff6-b65e-561fe04b96a0"
            it[cost] = BigDecimal("150")
        }

        MenuItems.insert {
            it[menuItemGUID] = "81a76c31-50bd-4a9b-8025-738ee8e35bd3"
            it[dishGUID] = insertedDishesGuids[4]
            it[menuGUID] = "228c90d6-ca0b-4ff6-b65e-561fe04b96a0"
            it[cost] = BigDecimal("300")
        }

        MenuItems.insert {
            it[menuItemGUID] = "0842df58-af64-4196-a9aa-ad9e8bd93794"
            it[dishGUID] = insertedDishesGuids[5]
            it[menuGUID] = "81f2fa41-6d02-4c3a-92bb-b2b92d177ae3"
            it[cost] = BigDecimal("150")
        }

        MenuItems.insert {
            it[menuItemGUID] = "b8d66b1d-3d19-42e4-981a-09bacc429303"
            it[dishGUID] = insertedDishesGuids[6]
            it[menuGUID] = "81f2fa41-6d02-4c3a-92bb-b2b92d177ae3"
            it[cost] = BigDecimal("150")
        }

        Orders.insert {
            it[orderGUID] = "ea35f027-91c2-43e5-928d-9ed91125c3b3"
            it[clientGUID] = "06c0821f-e0ce-4a3d-9fd8-4ca26cc19ce0"
            it[status] = StatusEnum.Created
            it[payment] = PaymentEnum.Cash
            it[comment] = "Домофон не работает"
        }

        Orders.insert {
            it[orderGUID] = "c766d298-f8cb-48d1-848d-f142e893031f"
            it[clientGUID] = "2fae5b78-f28f-4381-85e7-fbcded4eeb47"
            it[status] = StatusEnum.Created
            it[payment] = PaymentEnum.Cash
            it[comment] = "Домофон уже работает"
        }

        OrderItems.insert {
            it[menuItemGUID] = "1c6f76d4-3369-4cc4-b498-548fd7469433"
            it[orderGUID] = "c766d298-f8cb-48d1-848d-f142e893031f"
            it[preference] = "Не плюйте в блюдо"
            it[count] = BigDecimal("3")
        }

        Orders.insert {
            it[orderGUID] = "69acf991-b29d-43e9-8c0c-5b94e3bad824"
            it[clientGUID] = "2fae5b78-f28f-4381-85e7-fbcded4eeb47"
            it[status] = StatusEnum.Cooking
            it[payment] = PaymentEnum.Card
        }

        OrderItems.insert {
            it[menuItemGUID] = "1c6f76d4-3369-4cc4-b498-548fd7469433"
            it[orderGUID] = "69acf991-b29d-43e9-8c0c-5b94e3bad824"
            it[preference] = "Не плюйте в блюдо"
            it[count] = BigDecimal("3")
        }

        OrderItems.insert {
            it[menuItemGUID] = "81a76c31-50bd-4a9b-8025-738ee8e35bd3"
            it[orderGUID] = "69acf991-b29d-43e9-8c0c-5b94e3bad824"
            it[preference] = "Плюйте в блюдо"
            it[count] = BigDecimal("10")
        }



        Orders.insert {
            it[orderGUID] = "c9d838fc-a855-47fb-976f-09ef9b294325"
            it[clientGUID] = "06c0821f-e0ce-4a3d-9fd8-4ca26cc19ce0"
            it[status] = StatusEnum.Cooking
            it[payment] = PaymentEnum.Card
            it[comment] = "Домофон уже работает"
        }

        Orders.insert {
            it[orderGUID] = "0f9d4e85-d56a-4022-83ce-235513119e14"
            it[clientGUID] = "ae700e15-5d76-4396-b462-3072392278ae"
            it[status] = StatusEnum.Created
            it[payment] = PaymentEnum.Card
        }

        Orders.insert {
            it[orderGUID] = "bde702e9-6255-4c54-a0c5-c01b5c3f85e2"
            it[clientGUID] = "06c0821f-e0ce-4a3d-9fd8-4ca26cc19ce0"
            it[status] = StatusEnum.Done
            it[deliverGuid] = "6f1df7f0-32d1-40a7-a13d-917d2b8e5957"
            it[payment] = PaymentEnum.Card
            it[comment] = "Оставьте у двери"
        }

        Orders.insert {
            it[orderGUID] = "bde702e9-6255-4c54-a0c5-c01b5c3f85e3"
            it[clientGUID] = "06c0821f-e0ce-4a3d-9fd8-4ca26cc19ce0"
            it[status] = StatusEnum.WaitingTakeout
            it[deliverGuid] = "6f1df7f0-32d1-40a7-a13d-917d2b8e5957"
            it[payment] = PaymentEnum.Card
            it[comment] = "Оставьте у двери"
        }

        OrderItems.insert {
            it[menuItemGUID] = "81a76c31-50bd-4a9b-8025-738ee8e35bd3"
            it[orderGUID] = "bde702e9-6255-4c54-a0c5-c01b5c3f85e3"
            it[preference] = "Плюйте в блюдо"
            it[count] = BigDecimal("1")
        }

        Orders.insert {
            it[orderGUID] = "97030c25-a553-4cb7-abfa-6339e440cc83"
            it[clientGUID] = "06c0821f-e0ce-4a3d-9fd8-4ca26cc19ce0"
            it[status] = StatusEnum.Delivering
            it[payment] = PaymentEnum.Card
            it[deliverGuid] = "6f1df7f0-32d1-40a7-a13d-917d2b8e5957"
        }

        OrderItems.insert {
            it[menuItemGUID] = "1c6f76d4-3369-4cc4-b498-548fd7469433"
            it[orderGUID] = "ea35f027-91c2-43e5-928d-9ed91125c3b3"
            it[preference] = "Не плюйте в блюдо"
            it[count] = BigDecimal("2")
        }

        OrderItems.insert {
            it[menuItemGUID] = "81a76c31-50bd-4a9b-8025-738ee8e35bd3"
            it[orderGUID] = "ea35f027-91c2-43e5-928d-9ed91125c3b3"
            it[preference] = "Без волос, пожалуйста"
            it[count] = BigDecimal("1")
        }

        OrderItems.insert {
            it[menuItemGUID] = "1c6f76d4-3369-4cc4-b498-548fd7469433"
            it[orderGUID] = "c9d838fc-a855-47fb-976f-09ef9b294325"
            it[preference] = "Не солите"
            it[count] = BigDecimal("5")
        }

        OrderItems.insert {
            it[menuItemGUID] = "a58efee1-9ad7-4007-8fcc-1426b0f41a56"
            it[orderGUID] = "c9d838fc-a855-47fb-976f-09ef9b294325"
            it[preference] = "Не перчите"
            it[count] = BigDecimal("2")
        }

        OrderItems.insert {
            it[menuItemGUID] = "1c6f76d4-3369-4cc4-b498-548fd7469433"
            it[orderGUID] = "0f9d4e85-d56a-4022-83ce-235513119e14"
            it[preference] = "Не солите"
            it[count] = BigDecimal("5")
        }

        OrderItems.insert {
            it[menuItemGUID] = "a58efee1-9ad7-4007-8fcc-1426b0f41a56"
            it[orderGUID] = "0f9d4e85-d56a-4022-83ce-235513119e14"
            it[count] = BigDecimal("2")
        }

        OrderItems.insert {
            it[menuItemGUID] = "b8d66b1d-3d19-42e4-981a-09bacc429303"
            it[orderGUID] = "bde702e9-6255-4c54-a0c5-c01b5c3f85e2"
            it[preference] = "Без тараканов"
            it[count] = BigDecimal("3")
        }

        OrderItems.insert {
            it[menuItemGUID] = "a58efee1-9ad7-4007-8fcc-1426b0f41a56"
            it[orderGUID] = "97030c25-a553-4cb7-abfa-6339e440cc83"
            it[preference] = "Посолить от души"
            it[count] = BigDecimal("4")
        }

        OrderItems.insert {
            it[menuItemGUID] = "81a76c31-50bd-4a9b-8025-738ee8e35bd3"
            it[orderGUID] = "bde702e9-6255-4c54-a0c5-c01b5c3f85e2"
            it[count] = BigDecimal("20")
        }
    }

    suspend fun <T> dbQuery(block: () -> T): T =
        withContext(Dispatchers.IO) {
            transaction { block() }
        }
}