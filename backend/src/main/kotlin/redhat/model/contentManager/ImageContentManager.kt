package redhat.model.contentManager

import io.ktor.http.content.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.math.BigInteger
import java.security.MessageDigest

object ImageContentManager
{
    private lateinit var rootPath: String
    private val imageHasher = MessageDigest.getInstance("MD5")

    fun setFilesPath(path: String){
        rootPath = path
    }

    suspend fun saveFiles(files: List<Pair<String, PartData.FileItem>>, folderName: String) {
        val path = "$rootPath/$folderName"
        File(path).mkdir()
        files.forEach{ fileData ->
            saveImage(
                file = fileData.second,
                path = path,
                fileName = fileData.first
            )
        }
    }

    private suspend fun saveImage(file: PartData.FileItem, path: String, fileName: String) {
        withContext(Dispatchers.Default) {
            val fileBytes = file.streamProvider().readBytes()
            File("$path/$fileName").writeBytes(fileBytes)
        }
    }

    fun calculateFileName(file: PartData.FileItem) : String {
        return "${BigInteger(1, imageHasher.digest(file.streamProvider().readBytes()))
            .toString(16).padStart(32, '0')}.${file.originalFileName?.takeLastWhile { it != '.' }}"
    }
}