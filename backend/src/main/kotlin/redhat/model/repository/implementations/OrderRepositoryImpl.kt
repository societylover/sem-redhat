package redhat.model.repository.implementations

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import redhat.model.DatabaseInitialize.dbQuery
import redhat.model.enums.PaymentEnum
import redhat.model.enums.StatusEnum
import redhat.model.repository.OrderRepository
import redhat.model.repository.OrderRepository.Companion.CreatingOrderData
import redhat.model.repository.OrderRepository.Companion.ShowOrderShortData
import redhat.model.repository.OrderRepository.Companion.ShowOrderFullData
import redhat.model.repository.OrderRepository.Companion.ShowOrderItemFullData
import redhat.model.tables.*
import java.math.BigDecimal
import java.util.UUID

class OrderRepositoryImpl : OrderRepository {

    private suspend fun checkOrderItems(itemsGuid: List<String>): Boolean {
        return dbQuery {

            itemsGuid.map {
                val foundItem = MenuItems.select { MenuItems.menuItemGUID.eq(it) }.firstOrNull()
                if (foundItem == null) {
                    false
                } else {
                    foundItem[MenuItems.available]
                }
            }.all { it }
        }
    }

    override suspend fun createOrder(creatingOrderData: CreatingOrderData): String? {

        val checkItems =
            suspendedTransactionAsync {
                checkOrderItems(creatingOrderData.orderItems.map { it.menuItemGUID })
            }.await()

        if (!checkItems) {
            return null
        }

        return dbQuery {
            val orderGuid = Orders.insert {
                it[Orders.orderGUID] = UUID.randomUUID().toString()
                it[Orders.clientGUID] = creatingOrderData.clientGUID
                it[Orders.comment] = creatingOrderData.comment
                it[Orders.created] = CurrentDateTime
                it[Orders.payment] = PaymentEnum.valueOf(creatingOrderData.payment)
                it[Orders.status] = StatusEnum.Created
            } get Orders.orderGUID

            for (orderItem in creatingOrderData.orderItems) {
                OrderItems.insert {
                    it[OrderItems.orderGUID] = orderGuid
                    it[OrderItems.menuItemGUID] = orderItem.menuItemGUID
                    it[OrderItems.count] = BigDecimal(orderItem.count)
                    it[OrderItems.preference] = orderItem.preference
                }
            }
            orderGuid
        }
    }


    override suspend fun getOrdersShortData(
        userId: String?,
        searchingStatuses: List<StatusEnum>
    ): List<ShowOrderShortData> {
        return dbQuery {
            // ID заказа, дата, адрес и статус
            if (userId == null) // use only status filter
            {
                Orders.select { Orders.status.inList(searchingStatuses) }
                    .map {
                        ShowOrderShortData(
                            orderGuid = it[Orders.orderGUID],
                            created = it[Orders.created].toString(),
                            address = Clients.select { Clients.clientGUID.eq(it[Orders.clientGUID]) }
                                .first()[Clients.address],
                            status = it[Orders.status]
                        )
                    }
            } else {
                val clientsData = Clients.select { Clients.userGUID.eq(userId) }.map {
                    Pair(it[Clients.clientGUID], it[Clients.address])
                }

                println(clientsData)

                Orders.select { Orders.clientGUID.inList(clientsData.map { it.first }) }
                    .map {
                        ShowOrderShortData(
                            orderGuid = it[Orders.orderGUID],
                            created = it[Orders.created].toString(),
                            address = clientsData.first { data -> data.first == it[Orders.clientGUID] }.second,
                            status = it[Orders.status]
                        )
                    }
            }
        }
    }

    override suspend fun getOrderFullData(orderGuid: String): ShowOrderFullData {
        return dbQuery {
            Orders.select { Orders.orderGUID.eq(orderGuid) }
                .map {
                    ShowOrderFullData(
                        orderGuid = it[Orders.orderGUID],
                        created = it[Orders.created].toString(),
                        address = Clients.select { Clients.clientGUID.eq(it[Orders.clientGUID]) }
                            .first()[Clients.address],
                        status = it[Orders.status],
                        comment = it[Orders.comment],
                        payment = it[Orders.payment],
                        items = OrderItems.select { OrderItems.orderGUID.eq(it[Orders.orderGUID]) }.map { item ->

                            val menuItemData =
                                MenuItems.select { MenuItems.menuItemGUID.eq(item[OrderItems.menuItemGUID]) }.first()

                            ShowOrderItemFullData(
                                menuItemGUID = item[OrderItems.menuItemGUID],
                                count = item[OrderItems.count].toPlainString(),
                                preference = item[OrderItems.preference],
                                itemName = Dishes.select { Dishes.dishGUID.eq(menuItemData[MenuItems.dishGUID]) }
                                    .first()[Dishes.name],
                                pricePerPiece = (menuItemData[MenuItems.cost] - menuItemData[MenuItems.discount]).toPlainString()
                            )
                        }
                    )
                }.single()
        }
    }

    override suspend fun getCourierOrdersShortData(courierId: String): List<ShowOrderShortData> {
        return dbQuery {
            // ID заказа, дата, адрес и статус
            Orders.select { Orders.deliverGuid.eq(courierId) or Orders.status.eq(StatusEnum.WaitingTakeout) }
                .map {
                    ShowOrderShortData(
                        orderGuid = it[Orders.orderGUID],
                        created = it[Orders.created].toString(),
                        address = Clients.select { Clients.clientGUID.eq(it[Orders.clientGUID]) }
                            .first()[Clients.address],
                        status = it[Orders.status]
                    )
                }
        }
    }

    override suspend fun updateOrderStatus(orderGuid: String, status: StatusEnum) {
        return dbQuery {
            Orders.update({ Orders.orderGUID.eq(orderGuid) }) {
                it[Orders.status] = status
            }
        }
    }

    override suspend fun takeOrderInDelivery(orderGuid: String, courierId: String) {
        return dbQuery {
            transaction {
                Orders.update({ Orders.orderGUID.eq(orderGuid) }) {
                    it[Orders.status] = StatusEnum.Delivering
                    it[Orders.deliverGuid] = courierId
                }
            }
        }
    }
}