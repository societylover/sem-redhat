package redhat.model.repository.implementations

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.experimental.suspendedTransactionAsync
import redhat.model.DatabaseInitialize.dbQuery
import redhat.model.dto.User
import redhat.model.enums.Role
import redhat.model.repository.UserRepository
import redhat.model.tables.Users
import java.util.*
import kotlin.math.log

class UserRepositoryImpl : UserRepository {
    override suspend fun createUser(login: String, passwordHash: String, role: Role): User {
        suspendedTransactionAsync {
            dbQuery {
                Users.insert {
                    it[Users.login] = login
                    it[Users.hashedPassword] = passwordHash
                    it[Users.role] = role
                }

            }
        }.await()

        return getUserByCredentials(login)!!
    }

    override suspend fun getUserByCredentials(login: String): User? {
        return dbQuery {
            Users.select { Users.login.eq(login) }
                .map {
                    User(
                        userGUID = it[Users.userGUID],
                        login = it[Users.login],
                        passwordHash = it[Users.hashedPassword],
                        userRole = it[Users.role]
                    )
                }
        }.firstOrNull()
    }

    override suspend fun checkIfUserExistByCredentials(login: String): Boolean {
        return dbQuery {
            !Users.select { Users.login.eq(login) }.empty()
        }
    }

    override suspend fun getUserByGUID(userGuid: String): User? {
        return dbQuery {
            Users.select { Users.userGUID.eq(userGuid) }
                .map {
                    User(
                        userGUID = it[Users.userGUID],
                        login = it[Users.login],
                        passwordHash = it[Users.hashedPassword],
                        userRole = it[Users.role]
                    )
                }
        }.singleOrNull()
    }

    override fun deleteUserByGUID(userGUID: UUID) {
        TODO("Not yet implemented")
    }
}