package redhat.model.repository.implementations

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import redhat.model.DatabaseInitialize.dbQuery
import redhat.model.repository.ClientRepository
import redhat.model.tables.Clients
import redhat.model.repository.ClientRepository.Companion.ClientAddress
import redhat.model.tables.Orders
import java.util.UUID

class ClientRepositoryImpl : ClientRepository {

    override suspend fun getClientAddressesByUserGuid(userGUID: String): List<ClientAddress> {
        return dbQuery {
            Clients.select { Clients.userGUID.eq(userGUID) and Clients.isDeleted.eq(false) }
                .map {
                    ClientAddress(
                        clientGuid = it[Clients.clientGUID],
                        address = it[Clients.address]
                    )
                }
        }
    }

    override suspend fun createClientAddressByUserGuid(userGUID: String, address: String) {
        return dbQuery {
            Clients.insert {
                it[Clients.address] = address
                it[Clients.userGUID] = userGUID
                it[Clients.clientGUID] = UUID.randomUUID().toString()
            }
        }
    }

    override suspend fun deleteClientByGuid(clientGuid: String) {
        return dbQuery {
            Clients.update ({ Clients.clientGUID.eq(clientGuid) }){
                it[Clients.isDeleted] = true
            }
        }
    }
}