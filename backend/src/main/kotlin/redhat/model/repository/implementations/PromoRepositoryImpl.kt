package redhat.model.repository.implementations

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.update
import redhat.model.DatabaseInitialize.dbQuery
import redhat.model.repository.PromoRepository
import redhat.model.tables.Promos
import redhat.model.repository.PromoRepository.Companion.FullPromoItem

class PromoRepositoryImpl : PromoRepository {

    override suspend fun addPromoImages(imagesFullPaths: List<String>) {
        return dbQuery {
            imagesFullPaths.forEach { path ->
                Promos.insert {
                    it[Promos.promoImageUri] = path
                }
            }
        }
    }

    override suspend fun setShownPromoItems(imagesFullPath: List<String>) {
        return dbQuery {

            Promos.update({ Promos.promoImageUri.inList(imagesFullPath) }) {
                it[Promos.isShown] = true
            }

            Promos.update({ Promos.promoImageUri.notInList(imagesFullPath) }) {
                it[Promos.isShown] = false
            }
        }
    }

    override suspend fun getAllPromosData(): List<FullPromoItem> {
        return dbQuery {
            Promos.selectAll().map {
                FullPromoItem(
                    filePath = it[Promos.promoImageUri],
                    isShown = it[Promos.isShown]
                )
            }
        }
    }

    override suspend fun getCurrentPromoItemsPath(): List<String> {
        return dbQuery {
            Promos.select { Promos.isShown.eq(true) }.map {
                it[Promos.promoImageUri]
            }
        }
    }
}