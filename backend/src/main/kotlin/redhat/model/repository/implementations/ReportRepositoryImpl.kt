package redhat.model.repository.implementations

import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.update
import redhat.model.DatabaseInitialize.dbQuery
import redhat.model.repository.ReportRepository
import redhat.model.tables.Reports
import redhat.model.repository.ReportRepository.Companion.Report
import redhat.model.tables.ReportPhotos
import java.util.UUID

class ReportRepositoryImpl(private val reportFilesRoot: String) : ReportRepository {

    override suspend fun createReport(userGUID: String, text: String, photoUris: List<String>, result: String?) : String {
        return dbQuery {

            val reportGuid = UUID.randomUUID().toString()

            Reports.insert {
                it[Reports.reportGUID] = reportGuid
                it[Reports.userGUID] = userGUID
                it[Reports.created] = CurrentDateTime
                it[Reports.text] = text
                it[Reports.result] = result
            }

            insertPhotos(reportGuid, photoUris)

            reportGuid
        }
    }

    private fun insertPhotos(reportGuid: String, photoUris: List<String>) {
        photoUris.forEach { photoUri ->
            ReportPhotos.insert {
                it[ReportPhotos.reportGUID] = reportGuid
                it[ReportPhotos.photoUri] = "$reportFilesRoot/$reportGuid/$photoUri"
            }
        }
    }

    private fun getReportPhotos(reportGuid: String): List<String> {
        return ReportPhotos.select { ReportPhotos.reportGUID.eq(reportGuid) }
            .map {
                it[ReportPhotos.photoUri]
            }
    }
    override suspend fun processReport(reportGUID: String, result: String) {
        return dbQuery {
            Reports.update({ Reports.reportGUID.eq(reportGUID) }) {
                it[Reports.result] = result
            }
        }
    }

    override suspend fun getUserReports(userGUID: String): List<Report> {

        return dbQuery {
            Reports.select { Reports.userGUID.eq(userGUID) }.map {
                val reportGuid = it[Reports.reportGUID]
                Report(
                    reportGUID = reportGuid,
                    text = it[Reports.text],
                    userGUID = userGUID,
                    created = it[Reports.created].toString(),
                    photoUris = getReportPhotos(reportGuid),
                    result = it[Reports.result]
                )
            }
        }
    }
}