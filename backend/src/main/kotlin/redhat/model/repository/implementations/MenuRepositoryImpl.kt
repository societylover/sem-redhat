package redhat.model.repository.implementations

import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import redhat.model.DatabaseInitialize.dbQuery
import redhat.model.dto.MenuItemData
import redhat.model.repository.MenuRepository
import redhat.model.tables.Dishes
import redhat.model.tables.MenuItems
import redhat.model.repository.MenuRepository.Companion.ValidatedCartItem
import java.math.BigDecimal
import redhat.model.repository.MenuRepository.Companion.CafeMenu
import redhat.model.tables.Menus

class MenuRepositoryImpl : MenuRepository {

    private lateinit var currentMenu: String
    private lateinit var menu: List<MenuItemData>

    override fun setCurrentMenu(menuId: String) {
        currentMenu = menuId
        runBlocking {
            updateCurrentMenu()
        }
    }

    private suspend fun updateCurrentMenu() {
        menu = dbQuery {

            MenuItems.select { MenuItems.menuGUID.eq(currentMenu) }
                .map { menuItem ->

                    val dish = Dishes.select { Dishes.dishGUID.eq(menuItem[MenuItems.dishGUID]) }.single()

                    MenuItemData(
                        menuItemGuid = menuItem[MenuItems.menuItemGUID],
                        name = dish[Dishes.name],
                        description = dish[Dishes.description],
                        photoUri = dish[Dishes.pictureUrl],
                        available = menuItem[MenuItems.available],
                        cost = menuItem[MenuItems.cost].toString(),
                        discount = menuItem[MenuItems.discount].toString()
                    )
                }
        }
    }


    override suspend fun getCurrentMenuItems(): List<MenuItemData> {
        check(this::currentMenu.isInitialized)
        return menu
    }

    override suspend fun calculateCart(items: List<String>): List<ValidatedCartItem> {
        return dbQuery {
            val result = mutableListOf<ValidatedCartItem>()

            for (itemGuid in items) {
                val item = menu.firstOrNull { it.menuItemGuid == itemGuid }

                if (item == null) {
                    result.add(ValidatedCartItem(menuItem = itemGuid))
                    continue
                }
                result.add(
                    ValidatedCartItem(
                        menuItem = item.menuItemGuid,
                        available = item.available,
                        pricePerUnit = (BigDecimal(item.cost) - BigDecimal(item.discount)).toPlainString()
                    )
                )
            }
            result
        }
    }

    override suspend fun getMenus(): List<CafeMenu> {
        return dbQuery {
            Menus.selectAll().map {
            val menuGuid = it[Menus.menuGUID]
                CafeMenu(
                    menuGuid = menuGuid,
                    menuName = it[Menus.name],
                    createdAt = it[Menus.createdAt].toString(),
                    isCurrentMenu = (menuGuid == currentMenu)
                )
            }
        }
    }
}