package redhat.model.repository

import kotlinx.serialization.Serializable

interface PromoRepository {
    suspend fun addPromoImages(imagesFullPaths: List<String>)
    suspend fun setShownPromoItems(imagesFullPath: List<String>)
    suspend fun getAllPromosData() : List<FullPromoItem>
    suspend fun getCurrentPromoItemsPath() : List<String>

    companion object {
        @Serializable
        data class FullPromoItem(
            val filePath: String,
            val isShown: Boolean
        )
    }
}