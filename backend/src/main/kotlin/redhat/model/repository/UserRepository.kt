package redhat.model.repository

import redhat.model.dto.User
import redhat.model.enums.Role
import java.util.StringJoiner
import java.util.UUID

interface UserRepository {
    suspend fun createUser(login: String, passwordHash: String, role: Role = Role.Client) : User
    suspend fun getUserByCredentials(login: String) : User?
    suspend fun checkIfUserExistByCredentials(login: String) : Boolean
    suspend fun getUserByGUID(userGuid: String) : User?
    fun deleteUserByGUID(userGUID: UUID)
}