package redhat.model.repository

import kotlinx.serialization.Serializable

interface ClientRepository {
    suspend fun getClientAddressesByUserGuid(userGUID: String) : List<ClientAddress>

    suspend fun createClientAddressByUserGuid(userGUID: String, address: String)

    suspend fun deleteClientByGuid(clientGuid: String)

    companion object {

        @Serializable
        data class ClientAddress(
            val clientGuid: String,
            val address: String
        )
    }
}