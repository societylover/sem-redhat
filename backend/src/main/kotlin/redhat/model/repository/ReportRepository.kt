package redhat.model.repository

import kotlinx.serialization.Serializable

interface ReportRepository {
    suspend fun createReport(userGUID: String, text: String, photoUris: List<String>, result: String? = null) : String
    suspend fun processReport(reportGUID: String, result: String)
    suspend fun getUserReports(userGUID: String): List<Report>

    companion object {
        @Serializable
        data class Report(
            val reportGUID: String,
            val text: String,
            val userGUID: String,
            val created: String,
            val photoUris: List<String>,
            val result: String? = null
        )
    }
}