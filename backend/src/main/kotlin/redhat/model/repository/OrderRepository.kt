package redhat.model.repository

import kotlinx.serialization.Serializable
import redhat.model.enums.PaymentEnum
import redhat.model.enums.StatusEnum

interface OrderRepository {
    suspend fun createOrder(creatingOrderData: CreatingOrderData) : String?
    suspend fun getOrdersShortData(userId: String? = null, searchingStatuses: List<StatusEnum> = StatusEnum.values().toList()) : List<ShowOrderShortData>
    suspend fun getOrderFullData(orderGuid: String) : ShowOrderFullData
    suspend fun getCourierOrdersShortData(courierId: String) : List<ShowOrderShortData>
    suspend fun updateOrderStatus(orderGuid: String, status: StatusEnum)
    suspend fun takeOrderInDelivery(orderGuid: String, courierId: String)

    companion object {
        @Serializable
        data class CreatingOrderData(
            val clientGUID: String,
            val payment: String,
            val comment: String? = "",
            val orderItems: List<CreatingOrderItem>
        )

        @Serializable
        data class CreatingOrderItem(
            val menuItemGUID: String,
            val count: String,
            val preference: String? = ""
        )

        @Serializable
        data class ShowOrderShortData(
            val orderGuid: String,
            val created: String,
            val address: String,
            val status: StatusEnum
        )

        @Serializable
        data class ShowOrderFullData(
            val orderGuid: String,
            val created: String,
            val address: String,
            val status: StatusEnum,
            val payment: PaymentEnum,
            val comment: String? = "",
            val items: List<ShowOrderItemFullData>
        )

        @Serializable
        data class ShowOrderItemFullData(
            val menuItemGUID: String,
            val itemName: String,
            val count: String,
            val preference: String? = "",
            val pricePerPiece: String
        )
    }

}