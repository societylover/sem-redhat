package redhat.model.repository

import kotlinx.serialization.Serializable
import redhat.model.dto.MenuItemData

interface MenuRepository {
    fun setCurrentMenu(menuId: String)
    suspend fun getCurrentMenuItems() : List<MenuItemData>
    suspend fun calculateCart(items: List<String>) : List<ValidatedCartItem>
    suspend fun getMenus() : List<CafeMenu>

    companion object {
        @Serializable
        data class ValidatedCartItem(
            val menuItem: String,
            val available: Boolean = false,
            val pricePerUnit: String = ""
        )

        @Serializable
        data class CafeMenu(
            val menuGuid: String,
            val menuName: String,
            val createdAt: String,
            val isCurrentMenu: Boolean,
        )
    }
}