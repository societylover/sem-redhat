package redhat.model.tables

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.javatime.datetime
import redhat.model.enums.PaymentEnum
import redhat.model.enums.StatusEnum
import java.util.UUID

@OptIn(ExperimentalStdlibApi::class)
object Orders : Table() {
    val orderGUID = text("orderGUID").default(UUID.randomUUID().toString())
    val clientGUID = text("clientGUID").references(Clients.clientGUID)
    val status = enumerationByName("orderStatus", StatusEnum.entries.maxBy { it.name.length }.name.length, StatusEnum::class)
    val created = datetime("created").defaultExpression(CurrentDateTime)
    val payment = enumerationByName("payment", PaymentEnum.entries.maxBy { it.name.length }.name.length, PaymentEnum::class)
    val comment = text("comment").nullable()
    val deliverGuid = text("deliverGUID").references(Users.userGUID).nullable()
    override val primaryKey = PrimaryKey(orderGUID, name = "PK_OrderGUID")
}