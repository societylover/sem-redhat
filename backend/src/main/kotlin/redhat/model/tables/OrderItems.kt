package redhat.model.tables

import org.jetbrains.exposed.sql.Table

object OrderItems : Table() {
    val menuItemGUID = text("menuItemGUID").references(MenuItems.menuItemGUID)
    val orderGUID = text("orderGUID").references(Orders.orderGUID)
    val preference = text("preference").nullable()
    val count = decimal("count", scale = 2, precision = 10)
}