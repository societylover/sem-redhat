package redhat.model.tables

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.javatime.datetime
import org.jetbrains.exposed.sql.update
import java.util.UUID

object Menus : Table() {
    val menuGUID = text("menuGUID").default(UUID.randomUUID().toString())
    val name = text("name")
    val createdAt = datetime("createdAt").defaultExpression(CurrentDateTime)
    override val primaryKey = PrimaryKey(menuGUID, name = "PK_MenuGUID")
}