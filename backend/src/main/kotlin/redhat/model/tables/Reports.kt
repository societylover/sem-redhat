package redhat.model.tables

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.javatime.datetime
import java.util.UUID

object Reports : Table() {
    val reportGUID = text("reportGUID").default(UUID.randomUUID().toString())
    val text = text("text")
    val userGUID = text("userGUID").references(Users.userGUID)
    val created = datetime("created").defaultExpression(CurrentDateTime)
    val result = text("result").nullable()
    override val primaryKey = PrimaryKey(reportGUID, name = "PK_ReportGUID")
}