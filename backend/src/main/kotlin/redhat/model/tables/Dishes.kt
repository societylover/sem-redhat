package redhat.model.tables

import org.jetbrains.exposed.sql.Table
import java.util.UUID

object Dishes : Table() {
    val dishGUID = text("dishGUID").default(UUID.randomUUID().toString())
    val name = text("name")
    val pictureUrl = text("pictureUrl")
    val description = text("description")
    override val primaryKey = PrimaryKey(dishGUID, name = "PK_Dishes")
}