package redhat.model.tables

import org.jetbrains.exposed.sql.Table
import java.math.BigDecimal
import java.util.UUID

object MenuItems : Table() {
    val menuItemGUID = text("menuItemGUID").default(UUID.randomUUID().toString())
    val dishGUID = text("dishGUID").references(Dishes.dishGUID)
    val menuGUID = text("menuGUID").references(Menus.menuGUID)
    val available = bool("available").default(true)
    val cost = decimal("cost", scale = 2, precision = 10)
    val discount = decimal("discount", scale = 2, precision = 10).default(BigDecimal(0))
    override val primaryKey = PrimaryKey(menuItemGUID)
}