package redhat.model.tables

import org.jetbrains.exposed.sql.Table
import java.util.UUID

object Clients : Table() {
    val clientGUID = text("clientGUID").default(UUID.randomUUID().toString())
    val userGUID = text("userGUID").references(Users.userGUID)
    val address = text("address")
    val isDeleted = bool("isDeleted").default(false)
    override val primaryKey = PrimaryKey(clientGUID, name = "PK_ClientGUID")
}