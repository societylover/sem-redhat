package redhat.model.tables

import org.jetbrains.exposed.sql.Table

object ReportPhotos : Table() {
    val reportGUID = text("reportGUID").references(Reports.reportGUID)
    val photoUri = text("photoUri")
}