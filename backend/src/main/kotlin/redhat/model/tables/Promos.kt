package redhat.model.tables

import org.jetbrains.exposed.sql.Table

object Promos : Table() {
    val promoImageUri = text("promoItem")
    val isShown = bool("isShown").default(false)
}

