package redhat.model.tables

import org.jetbrains.exposed.sql.Table
import redhat.model.enums.Role
import java.util.UUID

object Users : Table() {
    val userGUID = text("userGUID").default(UUID.randomUUID().toString())
    val login = text("login")
    val hashedPassword = text("hashedPassword")
    @OptIn(ExperimentalStdlibApi::class)
    val role = enumerationByName("role", Role.entries.maxBy { it.name.length }.name.length, Role::class)
    override val primaryKey = PrimaryKey(userGUID, name = "PK_UserGUID")
}